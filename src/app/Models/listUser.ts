export class ListUser{

	apellido1:string;
  
	apellido2:string;
  
	nombre1:string;
  
	nombre2:string;
  
	paisEmpleo:string;
  
	tipoDocumento:string;
  
	numeroDocumento: string;
  
	correo: string;
  
	fechaIngreso: string;
  
	fechaRegistro: string;
  
	estado: boolean=true;

	fechaEdicion: Date;

	id:number;


}
