import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/Servicer/service.service';
import { CreateUser } from 'src/app/Models/createUser';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  listDependency:any[];


  constructor(private router:Router, private service:ServiceService) {
    let date=new Date();
    
    if((date.getMonth()+1)<10){
      if(date.getDay()<10){
        this.currentDate=date.getFullYear()+"-0"+(date.getMonth()+1)+"-0"+date.getDay();
        this.user.fechaRegistro=this.currentDate+'T'+date.getHours()+':'+date.getMinutes();
        date.setMonth(date.getMonth()-1);
        this.previousDate=date.getFullYear()+"-0"+(date.getMonth()+1)+"-0"+date.getDate();
      }else{
        this.currentDate=date.getFullYear()+"-0"+(date.getMonth()+1)+"-"+date.getDay();
        this.user.fechaRegistro=this.currentDate+'T'+date.getHours()+':'+date.getMinutes();
        date.setMonth(date.getMonth()-1);
        this.previousDate=date.getFullYear()+"-0"+(date.getMonth()+1)+"-"+date.getDate();
      
      }
      console.log(date.getDate())
      console.log(this.currentDate)
      console.log(this.previousDate)
    }
   }

  user:CreateUser=new CreateUser();
  currentDate='';
  previousDate='';
  currentDateTime='';

  ngOnInit() {
    console.log(this.user)
    this.user.estado=true;
  }

   save(){
    console.log(this.user)
    if(this.user.apellido1==null||this.user.apellido1==''){
      return alert("Debe agregar un primer apellido")
    }
    if(this.user.apellido2==null||this.user.apellido2==''){
      return alert("Debe agregar un segundo apellido")
    }
    if(this.user.nombre1==null||this.user.nombre1==''){
      return alert("Debe agregar un primer nombre")
    }
    if(this.user.paisEmpleo==null||this.user.paisEmpleo==''){
      return alert("Debe seleccionar un pais")
    }
    if(this.user.tipoDocumento==null||this.user.tipoDocumento==''){
      return alert("Debe seleccionar un tipo de documento")
    }
    if(this.user.numeroDocumento==null||this.user.numeroDocumento==''){
      return alert("Debe agregar un numero de documento")
    }
    if(this.user.fechaIngreso==null||this.user.fechaIngreso==''){
      return alert("Debe agregar una fecha de ingreso")
    }
    if(this.user.fechaRegistro==null||this.user.fechaRegistro==''){
      return alert("Debe agregar un numero de documento")
    }
     this.service.createUsers(this.user)
    .subscribe(data=>{
      if(data=="registro exitoso"){
        alert("Se registro con exito el empleado");
        this.router.navigate(["listar"]);
      }else{
        alert("El empleado ya existe");
      }
    },error=>{
      if(error.error.text=="registro exitoso"){
        alert("Se registro con exito el empleado");
        this.router.navigate(["listar"]);
      }else if(error.error.text=="ya existe"){
        alert("El empleado ya existe");
      }else{
        alert("Ocurrio un error registrando el empleado");
      }
    })
  } 

}
